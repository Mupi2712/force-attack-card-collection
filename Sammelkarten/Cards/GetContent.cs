﻿using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text.Json;
using Microsoft.UI.Xaml.Controls;
using Sammelkarten.Views;

namespace Sammelkarten.HTTP;

public class GetContent
{
    public List<List<Card>> Clonewars = new();
    public List<List<Card>> Movies = new();
    private int _seite = 1;
    public List<List<Card>> Combined {get{ 
        var combined = Clonewars.Select(it => it).ToList();
        combined.AddRange(Movies);
        return combined;
    }}
    private string Seite => $"_s{_seite}";
    private int serie = 1;
    private int ed = 2; // Grid
    private int itemsPerPage = 50; //af
    private Sorting order = Sorting.ItemNr; //Sortierung 7
    public event EventHandler OnUpdate;

    private string collection = "";
        //https://www.collect-it.de/Star-Wars-Force-Attax/Comic-Serien-Clone-Wars/Serie-4/Einzelkarten?Sortierung=7&af=200
    private string Url => $"https://www.collect-it.de/Star-Wars-Force-Attax/{collection}/Serie-{serie}/Einzelkarten{Seite}?Sortierung={order}&af={itemsPerPage}&ed={ed}";

    public GetContent()
    {
        try
        {
            var contentCw = File.ReadAllText("CloneWars.json");
            Clonewars = JsonSerializer.Deserialize<List<List<Card>>>(contentCw)??new List<List<Card>>();
        }catch(Exception ex){ Debug.WriteLine(ex); }
        try
        {
            var contentMovies = File.ReadAllText("Movies.json");
            Movies = JsonSerializer.Deserialize<List<List<Card>>>(contentMovies) ?? new List<List<Card>>();
        }catch(Exception ex){
            Debug.WriteLine(ex);
        }        
    }

    public void Update()
    {
        GetMovies();
        GetCloneWars();
        OnUpdate.Invoke(Combined, new());
    }
    #region Cw
    public void GetCloneWars()
    {
        var collections = new List<List<Card>>
        {
            GetCards(1, "Comic-Serien-Clone-Wars"),
            GetCards(2, "Comic-Serien-Clone-Wars"),
            GetCards(3, "Comic-Serien-Clone-Wars"),
            GetCards(4, "Comic-Serien-Clone-Wars"),
            GetCards(5, "Comic-Serien-Clone-Wars")
        };
        Clonewars = collections;
        File.WriteAllText("CloneWars.json",JsonSerializer.Serialize(collections));
    }
    #endregion
    #region Movies
    public void GetMovies()
    {
        var collections = new List<List<Card>>
        {
            GetCards(1, "Movie-Cards"),
            GetCards(2, "Movie-Cards"),
            GetCards(3, "Movie-Cards"),
    };
        Movies = collections;
        File.WriteAllText("Movies.json", JsonSerializer.Serialize(collections));
    }
    #endregion

    private List<Card> GetCards(int _serie, string _collection)
    {

        _seite = 1;
        serie = _serie;
        collection = _collection;
        var _list = new List<Card>();
        while (true)
        {
            var html = "";
            Task.Run(async () => html = await HttpServices.GetContent(Url)).Wait();
            _list.AddRange(HtmlSelector.GetData(html, out var isEnd, serie));
            if (isEnd)
                break;
            _seite++;
        }
        return _list;
    }

    #region ui
    public List<string> GetCollections()
    {
        var items = new List<string>();
        foreach(var item in Combined)
        {
            if(item.First().Collection == null) continue;
            if(!items.Contains(item.First().Collection!)) items.Add(item.First().Collection!);
        }
        return items;
    }

    internal List<Card> ListCards(int v)
    {
        if (!GetCollections().Any())
            return new();
        var collection = GetCollections()[v]?? GetCollections().FirstOrDefault();
        return Combined.First(it => it.First().Collection == collection).ToList();
    }
    #endregion
}
public enum Sorting{
    Az = 1, //1
    PriceAcs = 3,//3
    Newest = 6, //6
    ItemNr = 7,//7
    Date = 10,//10
    Bestseller = 11,//11
    Standard = 100, //100 == 7
    }

public class HttpServices
{
    public static async Task<string> GetContent(string url)
    {
        using HttpClient client = new();
        client.DefaultRequestHeaders.Accept.Clear();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html"));
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xhtml+xml"));
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("image/webp"));
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("image/apng"));
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/signed-exchange"));
        client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (TweetmemeBot/4.0; +http://datasift.com/bot.html) Gecko/20100101 Firefox/31.0");
        
        return await client.GetStringAsync(url);
    }
}