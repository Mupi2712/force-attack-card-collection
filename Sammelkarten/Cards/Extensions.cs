﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using HtmlAgilityPack;

namespace Sammelkarten.HTTP;

public static class Extensions
{
    #region getAttrib
    
    public static string GetHref(this HtmlNode node)
    {
        return node.Attributes["href"].Value;
    }
    public static string GetSrc(this HtmlNode node)
    {
        return node.Attributes["src"].Value;
    }
    public static string GetContent(this HtmlNode node)
    {
        return node.Attributes["content"].Value;
    }
    #endregion

    #region GetChilds

    public static HtmlNode LastDiv(this HtmlNode coll)
    {
        return coll.ChildNodes.Last(it => it.Name == "div");
    }
    public static HtmlNode FirstDiv(this HtmlNode coll)
    {
        return coll.ChildNodes.First(it => it.Name == "div");
    }
    public static HtmlNode LastTxt(this HtmlNode coll)
    {
        return coll.ChildNodes.Last(it => it.Name == "#text");
    }
    public static HtmlNode FirstTxt(this HtmlNode coll)
    {
        return coll.ChildNodes.First(it => it.Name == "#text");
    }
    
    public static HtmlNode LastForm(this HtmlNode coll)
    {
        return coll.ChildNodes.Last(it => it.Name == "form");
    }
    public static HtmlNode FirstForm(this HtmlNode coll)
    {
        return coll.ChildNodes.First(it => it.Name == "form");
    }
    
    public static HtmlNode LastA(this HtmlNode coll)
    {
        return coll.ChildNodes.Last(it => it.Name == "a");
    }
    public static HtmlNode FirstA(this HtmlNode coll)
    {
        return coll.ChildNodes.First(it => it.Name == "a");
    }
    
    public static HtmlNode LastMeta(this HtmlNode coll)
    {
        return coll.ChildNodes.Last(it => it.Name == "meta");
    }
    public static HtmlNode FirstMeta(this HtmlNode coll)
    {
        return coll.ChildNodes.First(it => it.Name == "meta");
    }
    
    public static HtmlNode LastImg(this HtmlNode coll)
    {
        return coll.ChildNodes.Last(it => it.Name == "img");
    }
    public static HtmlNode FirstImg(this HtmlNode coll)
    {
        return coll.ChildNodes.First(it => it.Name == "img");
    }
    #endregion

    public static int Count(this string source, string search)
    {
        var res = 0;
        var start = 0;
        var found = 0;
        while (found != -1)
        {
            found = source.IndexOf(search, start, StringComparison.Ordinal);
            if (found != -1)
                res++;
            start = found+1;
        }
        return res;
    }
    public static int TryParse(this string str)
    {
        try
        {
            return int.Parse(str);
        }catch (Exception ex)
        {
            Debug.WriteLine(ex);
            return 9999999;
        }
    }
}
