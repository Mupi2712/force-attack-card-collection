﻿using System.Text.Json.Serialization;
using Sammelkarten.Views;

namespace Sammelkarten.HTTP;

public class Card
{
    public string? ItemId
    {
        get; set;
    }
    public string? Name
    {
        get; set;
    }
    [JsonIgnore]
    public string PriceStr
    {
        get => $"{(Math.Round(Price, 2, MidpointRounding.ToEven))} {Currency}";
        private set
        {

        }
    }
    public double Price
    {
        get; set;
    }
    public string Currency
    {
        get; set;
    }
    public string Picture
    {
        get; set;
    }
    public string? Collection
    {
        get; set;
    }
    public string? Auflage
    {
        get; set;
    }
    public string? Group
    {
        get; set;
    }
    public string? Job
    {
        get; set;
    }
    public string Link
    {
        get; set;
    }
    public Card(float price, string currency, string picture, string link, string txt, int edition)
    {
        TrySetInfos(txt, edition, link);
        Price = price;
        Currency = currency;
        Picture = picture;
        Link = link;
    }

    public Card()
    {

    }

    private void TrySetInfos(string txt, int edition, string link)
    {
        if (link.Contains("FAMOV"))
        {
            switch (edition)
            {
                case 1:
                    Movie1(txt);
                    break;
                case 2:
                    Movie2(txt);
                    break;
                case 3:
                    Movie3(txt);
                    break;
            }
        }
        else
        {
            switch (edition)
            {
                case 1:
                    Clone1(txt);
                    break;
                case 2:
                    Clone2(txt);
                    break;
                case 3:
                    Clone3(txt);
                    break;
                case 4:
                    Clone4(txt);
                    break;
                case 5:
                    Clone5(txt);
                    break;
            }
        }
    }

    private void Movie1(string txt)
    {
        if (!txt.Contains(" - "))
        {
            Name = txt;
            return;
        }
        var infos = txt.Split(" - ");

        Collection = "Movie Serie 1 (2012)";
        ItemId = infos[0].Split('-')[1];
        Name = infos[1];

        switch (infos.Length)
        {
            case 2: //Strike Force
                Auflage = "Strike Force";
                break;
            case 3:
                Auflage = infos[2];
                break;
            case 4:
                if (infos.Last().Contains("Duell"))
                {
                    Group = infos[2];
                    Auflage = infos[3];
                }
                else
                {
                    Job = infos[2];
                    Group = infos[3];
                }
                break;
            case 5:
                Auflage = infos[2];
                Job = infos[3];
                Group = infos[4];
                break;
        }
    }

    private void Movie2(string txt)
    {
        if (!txt.Contains(" - "))
        {
            Name = txt;
            return;
        }
        var infos = txt.Split(" - ");

        Collection = "Movie Serie 2 (2013)";
        ItemId = infos[0].Split('-')[1];
        Name = infos[1];

        switch (infos.Length)
        {
            case 3: //LE
                Auflage = infos[2];
                break;
            case 4: //normal, duell, 3er, star, zusatz, fm, 
                if (infos.Last().Contains("Meister") || infos.Last().Contains("Zusatz") || infos.Last().Contains("Star"))
                {
                    Job = infos[2];
                    Auflage = infos[3];
                }
                if (infos.Last().Contains("Strike"))
                {
                    Auflage = infos[3];
                }
                if (infos.Last().Contains("Duell"))
                {
                    Group = infos[2];
                    Auflage = infos[3];
                }
                else
                {
                    Job = infos[2];
                    Group = infos[3];
                }
                break;
        }
    }

    private void Movie3(string txt)
    {
        if (!txt.Contains(" - "))
        {
            Name = txt;
            return;
        }
        var infos = txt.Split(" - ");

        Collection = "Movie Serie 3 (2014)";
        ItemId = infos[0].Split('-')[1];
        Name = infos[1];

        switch (infos.Length)
        {
            case 3: //LE
                if (infos.Last().Contains("Limitierte Auflage"))
                    Auflage = infos[2];
                else //3er
                {
                    Auflage = infos[1];
                    Name = infos[2];
                    Group = infos[2];
                }

                break;
            case 4: //normal
                Group = infos[2];
                Job = infos[3];
                break;
            case 5: //FM, star, zusatz
                Job = infos[2];
                Group = infos[3];
                Auflage = infos[4];
                break;

        }
    }

    private void Clone1(string txt)
    {
        if (!txt.Contains(" - "))
        {
            Name = txt;
            return;
        }
        var infos = txt.Split(" - ");

        Collection = "Clone Wars Serie 1 (2010)";
        ItemId = infos[0].Remove(0,2);
        Name = infos[1];

        switch (infos.Length)
        {
            case 3: //LE
                Name = Name.Replace("-", "");
                Auflage = infos[2];
                break;
            case 5: //FM, star, zusatz
                Job = infos[2];
                if (infos[3].Contains("Meister") || infos[3].Contains("Star") || infos[3].Contains("Strike") || infos[3].Contains("Zusatz"))
                    Auflage = infos[3];
                else
                    Group = infos[3];
                break;

        }
    }

    private void Clone2(string txt)
    {
        if (!txt.Contains(" - "))
        {
            Name = txt;
            return;
        }
        var infos = txt.Split(" - ");

        Collection = "Clone Wars Serie 2 (2011)";
        ItemId = infos[0].Split('-')[1];
        Name = infos[1];

        switch (infos.Length)
        {
            case 4://LE
                Auflage = infos[2];
                break;
            case 5: //3er
                if (infos[2].Contains("Strike"))
                {
                    Auflage = infos[2];
                    Group = infos[3];
                }
                else if (infos[3].Contains("Duell "))
                {
                    Group = infos[2];
                    Auflage = infos[3];
                }
                else
                {
                    Job = infos[2];
                    Group = infos[3];
                }
                break;
            case 6: //FM, zusatz, Star
                Job = infos[2];
                Group = infos[3];
                Auflage = infos[4];
                break;

        }
    }

    private void Clone3(string txt)
    {
        if (!txt.Contains(" - "))
        {
            Name = txt;
            return;
        }
        var infos = txt.Split(" - ");

        Collection = "Clone Wars Serie 3 (2012)";
        ItemId = infos[0].Split('-')[1];
        Name = infos[1];

        switch (infos.Length)
        {
            case 4://LE
                Auflage = infos[2];
                break;
            case 5: //3er, duell, FM
                if (infos[2].Contains("Strike"))
                {
                    Auflage = infos[2];
                    Group = infos[3];
                }
                else if (infos[3].Contains("Meister") || infos[3].Contains("Star"))
                {
                    Group = infos[2];
                    Auflage = infos[3];
                }
                else
                {
                    Job = infos[2];
                    Group = infos[3];
                }
                break;

        }
    }

    private void Clone4(string txt)
    {
        if (!txt.Contains(" - "))
        {
            Name = txt;
            return;
        }
        var infos = txt.Split(" - ");

        Collection = "Clone Wars Serie 4 (2013)";
        ItemId = infos[0].Split('-')[1];

        switch (infos.Length)
        {
            case 4://LE
                Name = infos[1];
                Auflage = infos[2];
                break;
            case 5: //3er
                if (infos[1].ToLower().Contains("strike"))
                {
                    Auflage = infos[1];
                    Name = infos[2];
                    Job = infos[2];
                    Group = infos[3];
                }
                else //normal
                {
                    Name = infos[1];
                    Job = infos[2];
                    Group = infos[3];
                }
                break;
            case 6: //star, FM
                Name = infos[1];
                Job = infos[2];
                Group = infos[3];
                Auflage = infos[4];
                break;
        }
    }

    private void Clone5(string txt)
    {
        if (!txt.Contains(" - "))
        {
            Name = txt;
            return;
        }
        var infos = txt.Split(" - ");
        if (infos[0].Split('-')[0] == "FA5")
        {
            Collection = "Clone Wars Serie 5 (2014)";
            ItemId = infos[0].Split('-')[1];
            switch (infos.Length)
            {
                case 4://STRIKE
                    if (infos[1].Contains("STRIKE"))
                    {
                        Auflage = "Strike Force";
                        Group = infos[2];
                        Name = infos[2];
                    }
                    else
                    {
                        Name = infos[1];
                        Auflage = infos[2];
                    }
                    break;
                case 5: //normal
                    Name = infos[1];
                    Job = infos[2];
                    Group = infos[3];
                    break;
                case 6: //star, FM
                    Name = infos[1];
                    Job = infos[2];
                    Group = infos[3];
                    Auflage = infos[4];
                    break;
            }
        }
        else //Spezialkarten
        {
            Collection = "Clone Wars Serie 5 (2014) - Spezial";
            ItemId = infos[0].Split('-')[1];
            switch (infos.Length)
            {
                case 5: //normal, star
                    Name = infos[1];
                    Job = infos[2];
                    Group = infos[3];
                    Auflage = infos[4];
                    break;
            }
        }

        
    }
}

public class CardDisplay : Card
{
    public bool HaveI
    {
        get;
        set;
    }
    public int HaveICount
    {
        get; set;
    }
    [JsonIgnore]
    public int Order => ItemId.TryParse();

    public CardDisplay() :base()
    {
        
    }

    public CardDisplay(Card wrapper, Tuple<string, bool, int>? have)
    {
        this.Auflage = wrapper.Auflage;
        this.Collection = wrapper.Collection;
        this.Currency = wrapper.Currency;
        this.ItemId = wrapper.ItemId;
        this.Group = wrapper.Group;
        this.Job = wrapper.Job;
        this.Link = wrapper.Link;
        this.Name = wrapper.Name;
        this.Picture = wrapper.Picture;
        this.Price = wrapper.Price;
        if( have != null)
        {
            this.HaveI = have.Item2;
            this.HaveICount = have.Item3;
        }
        else
        {
            this.HaveI = false;
            this.HaveICount = 1;
        }
    }
}
