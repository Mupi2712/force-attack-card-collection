﻿using HtmlAgilityPack;

namespace Sammelkarten.HTTP;

public class HtmlSelector
{
    public static List<Card> GetData(string html, out bool isEnd, int edition)
    {
        var res = new List<Card>();
        var doc = new HtmlDocument();
        doc.LoadHtml(html);
        //Seitenzahl
        var div = doc.GetElementbyId("result-wrapper");
        var sites = div.ChildNodes.Single(it => it.HasClass("productlist-page-nav-header-m"));
        var sites2 = sites.ChildNodes.Single(it => it.HasClass("productlist-item-info"));
        var txt = sites2.GetDirectInnerText();
        isEnd = IsEnd(txt);
        //product list
        var productList = doc.GetElementbyId("product-list");
        foreach(var node in productList.ChildNodes.Where(it=>it.ChildNodes.Count > 0))
        {
            var inner = node.LastDiv().LastForm().LastDiv();
            
            var linkNode = inner.FirstDiv().FirstDiv().FirstDiv().FirstDiv().LastA();//attr
            var link = linkNode.GetHref();
            var picture = linkNode.FirstDiv().FirstDiv().FirstImg().GetSrc();
            
            var _price =  inner.LastDiv().LastDiv().LastDiv().FirstDiv().FirstMeta().GetContent().Replace(",",".");
            var price = float.Parse(_price, System.Globalization.CultureInfo.InvariantCulture);
            var currency =inner.LastDiv().LastDiv().LastDiv().FirstDiv().LastMeta().GetContent();
            var text = inner.LastDiv().LastDiv().FirstDiv().LastA().GetDirectInnerText();

            text = text.Replace("&amp;", "&");
            if (link == "https://www.collect-it.de/FA-S00-Sparangebot-ALLE-48-Spezial-Karten")
                continue;
            res.Add(new Card(price, currency, picture, link,text.Trim(), edition));
                
        }
        return res;
    }
    private static bool IsEnd(string txt)
    {
        var startStart = txt.IndexOf("- ", StringComparison.Ordinal)+ 2;
        var endStart = txt.IndexOf("von", StringComparison.Ordinal) - 1;
        var curr = int.Parse(txt.Substring(startStart, endStart-startStart).Trim());
        var start = txt.IndexOf("von ", StringComparison.Ordinal)+4;
        var end = txt.IndexOf("\n", start, StringComparison.Ordinal);
        var to = int.Parse(txt.Substring(start, end-start).Trim());
        return curr == to;
    }
}

