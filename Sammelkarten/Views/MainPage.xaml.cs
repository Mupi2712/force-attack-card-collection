﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text.Json;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Media.Imaging;
using Sammelkarten.HTTP;
using Sammelkarten.ViewModels;

namespace Sammelkarten.Views;

public sealed partial class MainPage : Page
{
    private readonly GetContent content;
    private ObservableCollection<CardDisplay> Cards = new();
    private List<string> Collections => content.GetCollections();
    private List<Tuple<string, bool, int>> CardsClaimed = new();
    private bool IsInitialized = false;
    private SemaphoreSlim Semaphore = new(1, 1);
    private Dictionary<string, DataGridSortDirection> sortingDict = new();
    public MainViewModel ViewModel
    {
        get;
    }

    public MainPage()
    {
        CardsClaimed = RestoreHaveI();
        ViewModel = App.GetService<MainViewModel>();
        content = App.GetService<GetContent>();
        content.OnUpdate += Content_OnUpdate;
        Loaded += Window_Loaded;
        InitializeComponent();
        dataGrid1.AutoGeneratingColumn += Generating;
        dataGrid1.DoubleTapped += DoubleTab;
        dataGrid1.CellEditEnded += DataGrid1_CellEditEnded;
    }

    private void DoubleTab(object sender, DoubleTappedRoutedEventArgs e)
    {
        ContentDialog dialog = new ContentDialog();

        // XamlRoot must be set in the case of a ContentDialog running in a Desktop app
        dialog.XamlRoot = this.XamlRoot;
        dialog.Style = Application.Current.Resources["DefaultContentDialogStyle"] as Style;
        dialog.Title = "Details";
        dialog.PrimaryButtonText = "OK";
        dialog.DefaultButton = ContentDialogButton.Primary;
        var parent = (FrameworkElement)((FrameworkElement)e.OriginalSource).Parent;
        if (parent == null)
            return;
        dialog.Content = CreateCardInfo((CardDisplay)parent.DataContext);
        
        var result = dialog.ShowAsync();

    }

    private object CreateCardInfo(CardDisplay card)
    {
        var SP = new StackPanel() { Orientation = Orientation.Vertical, HorizontalAlignment = HorizontalAlignment.Center, };
        var img = new Image() { Source = new BitmapImage(new Uri(card.Picture)) };
        SP.Children.Add(img);

        var txt = new TextBlock() { FontSize= 22, HorizontalAlignment = HorizontalAlignment.Center, Text = $"{card.ItemId} {card.Name} {((card.Auflage == null || card.Auflage == "") ? "" : card.Auflage)}" };
        SP.Children.Add(txt);

        var btn = new HyperlinkButton() { FontSize = 22, HorizontalAlignment = HorizontalAlignment.Center, Content = "Open Page", NavigateUri = new Uri(card.Link) };
        SP.Children.Add(btn);
        return SP;
    }

    private void DataGrid1_CellEditEnded(object? sender, DataGridCellEditEndedEventArgs e)
    {
        if (e.Column.Header.ToString()!.ToLower().Contains("havei"))
        {
            if (!(e.EditAction == DataGridEditAction.Commit))
                return;
            if (!(e.Row.DataContext is CardDisplay dp))
                return;

            Save_Click(null, null);
        }
    }

    private List<Tuple<string, bool, int>> RestoreHaveI()
    {
        if (!File.Exists("HaveI.json"))
            return new();
        return JsonSerializer.Deserialize<List<Tuple<string, bool, int>>>(File.ReadAllText("HaveI.json")) ?? new();
    }

    private void Click(object sender, SelectionChangedEventArgs e)
    {
        ;
    }

    private void Generating(object? sender, DataGridAutoGeneratingColumnEventArgs e)
    {
        e.Column.Tag = e.PropertyName;
        string[] collapsed = { "picture", "price", "currency", "order" };
        string[] editable = { "link", "havei", "haveicount" };
        if (collapsed.Contains(e.PropertyName.ToLower()))
            e.Column.Visibility = Visibility.Collapsed;

        if (editable.Contains(e.PropertyName.ToLower()))
            e.Column.IsReadOnly = false;
        else
            e.Column.IsReadOnly = true;

    }
    private void Content_OnUpdate(object? sender, EventArgs e) => Window_Loaded(sender, null);

    public void Window_Loaded(object sender, RoutedEventArgs e)
    {
        combobox.Items.Clear();
        combobox.SelectedIndex = 0;
        foreach (var item in Collections)
            combobox.Items.Add(item);
        RefreshCards();
        IsInitialized = true;
    }

    private void combobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        Save_Click(sender, null);
        RefreshCards();
    }

    public void RefreshCards()
    {
        Cards.Clear();
        var _cards = new List<CardDisplay>();
        content.ListCards(combobox.SelectedIndex).ForEach(it => Cards.Add(new(it, GetHave(it.Link))));
    }

    private Tuple<string, bool, int>? GetHave(string link)
    {
        try
        {
            return CardsClaimed.Single(it => it.Item1 == link);
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex);
            return null;
        }
    }

    private void Save_Click(object sender, RoutedEventArgs e)
    {
        Semaphore.Wait();
        try
        {
            foreach (var item in Cards)
            {
                if (CardsClaimed.Any(it => it.Item1 == item.Link))
                    CardsClaimed.RemoveAll(it => it.Item1 == item.Link);
                CardsClaimed.Add(new Tuple<string, bool, int>(item.Link, item.HaveI, item.HaveICount));
            }

            File.WriteAllText("HaveI.json", JsonSerializer.Serialize(CardsClaimed));
            Message("Speichern erfolgreich!", "Erfolg!");
        }
        catch (Exception ex) { Debug.WriteLine(ex); }
        finally { Semaphore.Release(); }
    }

    private void Calc_Click(object sender, RoutedEventArgs e)
    {
        var dict = new Dictionary<string, string>();
        foreach (var coll in content.Combined)
        {
            var priceC = 0.0;
            foreach (var item in coll)
            {
                if (CardsClaimed.Any(it => it.Item1 == item.Link))
                {
                    var item2 = CardsClaimed.Single(it => it.Item1 == item.Link);
                    if (item2.Item2)
                        priceC += (item.Price * item2.Item3);
                }
            }
            dict.Add(coll.First().Collection!, priceC.ToString("0.##"));
        }
        var txt = "Pro Collection:\n\n";
        foreach (var item in dict)
            txt += $"{item.Key}: {item.Value} €\n";

        var price = 0.0;
        foreach (var item in CardsClaimed.Where(it => it.Item2))
        {
            try
            {
                foreach (var coll in content.Combined)
                {
                    if (coll.Any(it => it.Link == item.Item1))
                    {
                        var item2 = coll.Single(it => it.Link == item.Item1);
                        price += (item2.Price * item.Item3);
                    }

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        txt += $"\nTotal: {price:0.##} €";



        var dialog = new ContentDialog();

        // XamlRoot must be set in the case of a ContentDialog running in a Desktop app
        dialog.XamlRoot = XamlRoot;
        dialog.Style = Application.Current.Resources["DefaultContentDialogStyle"] as Style;
        dialog.Title = "Calculations";
        dialog.PrimaryButtonText = "OK";
        dialog.DefaultButton = ContentDialogButton.Primary;
        dialog.Content = new TextBlock() { Text = txt };

        dialog.ShowAsync();

    }

    private void Message(string message, string title)
    {
        if (!IsInitialized)
            return;
        info.Content = message;
        info.Title = title;
        info.IsOpen = true;
    }

    private void OnSorting(object sender, DataGridColumnEventArgs e)
    {
        //Implement sort on the column "Range" using LINQ
        if (!sortingDict.TryGetValue(e.Column.Tag.ToString()!, out var so))
            so = DataGridSortDirection.Descending;
        if (so == DataGridSortDirection.Descending)
        {
            switch (e.Column.Tag.ToString()?.ToLower())
            {
                case "auflage":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Auflage ascending select item);
                    break;
                case "collection":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Collection ascending select item);
                    break;
                case "itemid":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Order ascending select item);
                    break;
                case "group":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Group ascending select item);
                    break;
                case "job":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Job ascending select item);
                    break;
                case "link":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Link ascending select item);
                    break;
                case "pricestr":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Price ascending select item);
                    break;
                case "name":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Name ascending select item);
                    break;
                case "havei":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.HaveI ascending select item);
                    break;
                case "haveicount":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.HaveICount ascending select item);
                    break;
            }
            if (sortingDict.TryGetValue(e.Column.Tag.ToString()!, out var val))
                sortingDict.Remove(e.Column.Tag.ToString()!);
            sortingDict.Add(e.Column.Tag.ToString()!, DataGridSortDirection.Ascending);
            e.Column.SortDirection = DataGridSortDirection.Ascending;
        }
        else
        {
            switch (e.Column.Tag.ToString()?.ToLower())
            {
                case "auflage":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Auflage descending select item);
                    break;
                case "collection":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Collection descending select item);
                    break;
                case "itemid":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Order descending select item);
                    break;
                case "group":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Group descending select item);
                    break;
                case "job":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Job descending select item);
                    break;
                case "link":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Link descending select item);
                    break;
                case "name":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Name descending select item);
                    break;
                case "pricestr":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.Price descending select item);
                    break;
                case "havei":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.HaveI descending select item);
                    break;
                case "haveicount":
                    dataGrid1.ItemsSource = new ObservableCollection<CardDisplay>(from item in Cards orderby item.HaveICount descending select item);
                    break;
            }
            if (sortingDict.TryGetValue(e.Column.Tag.ToString()!, out var val))
                sortingDict.Remove(e.Column.Tag.ToString()!);
            sortingDict.Add(e.Column.Tag.ToString()!, DataGridSortDirection.Descending);
            e.Column.SortDirection = DataGridSortDirection.Descending;
        }
    }
}
